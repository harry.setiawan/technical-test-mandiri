package com.technical.test.mandiri.Module.Content.Fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Adapter.EndlessScrollListener;
import com.technical.test.mandiri.Component.Adapter.ListCategoriesAdapter;
import com.technical.test.mandiri.Component.Base.BaseFragmentPresenter;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Module.Content.ContentInteractor;
import com.technical.test.mandiri.Module.Content.ContentsView;
import com.technical.test.mandiri.R;

import butterknife.BindView;

public class FragmentNewsView extends BaseFragmentPresenter<NewsPresenter> implements
        ContentInteractor.ContentView.NewsView,
        SwipeRefreshLayout.OnRefreshListener,
        ListCategoriesAdapter.itemClickCallback {

    @BindView(R.id.refresh)
    SwipeRefreshLayout swRefresh;
    @BindView(R.id.recyclerView)
    public RecyclerView recyclerView;
    @BindView(R.id.tvNotFound)
    public TextView notFound;

    ListCategoriesAdapter NewsAdapter;
    ContentsView parentActivity;

    public FragmentNewsView(){ }

    public static FragmentNewsView newInstance()
    {
        return new FragmentNewsView();
    }

    @Override
    protected NewsPresenter initPresenter() {
        return new NewsPresenter(this);
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_news;
    }

    @Override
    protected void onInitialize(@Nullable Bundle savedInstanceState) {
        this.parentActivity = (ContentsView) getActivity();
        initTypeface();
        setSwipeRefreshColor();
        declareAdapter();
        parentActivity.currentPage = 0;
        mPresenter.OnRequestNews();
    }

    @Override
    public void initTypeface() {
        notFound.setTypeface(Utils.typefaceNormal());
    }

    @Override
    public void setSwipeRefreshColor() {
        swRefresh.setColorSchemeColors(getResources().getColor(R.color.md_blue_500));
        swRefresh.setOnRefreshListener(this);
    }

    @Override
    public void declareAdapter() {
        LinearLayoutManager LLayout = new GridLayoutManager(getActivity(), 1);
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LLayout = new GridLayoutManager(getActivity(), 4);
        } else {
            LLayout = new GridLayoutManager(getActivity(), 1);
        }
        recyclerView.setLayoutManager(LLayout);
        recyclerView.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(Apps.getInstance(), R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animation);
        NewsAdapter = new ListCategoriesAdapter(mPresenter.News, parentActivity);
        NewsAdapter.setItemClickCallback(this);
        recyclerView.setAdapter(NewsAdapter);
        recyclerView.addOnScrollListener(new EndlessScrollListener(LLayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if(Utils.isConnected()) {
                    parentActivity.currentPage += 1;
                    mPresenter.OnRequestNews();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        mPresenter.News.clear();
        if(Utils.isConnected()) {
            Apps.getInstance().getDatabase().ClearCategories();
        }
        parentActivity.currentPage = 0;
        mPresenter.OnRequestNews();
    }

    @Override
    public void onItemClick(int position) {
        Constant.selectDetilId = mPresenter.News.get(position).getCategoryId();
        Constant.DetilNews = true;
        parentActivity.mPresenter.OnLoadInitialDetilNewsFragment();
    }

    @Override
    public void onItemFavorClick(int position) {
    }
}
