package com.technical.test.mandiri.Module.Main.Fragment;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Module.Content.ContentsView;
import com.technical.test.mandiri.Module.Main.MainInteractor;

import static com.technical.test.mandiri.Apps.getInstance;

public class FragmentEnterPresenter extends BasePresenterImpl<MainInteractor.MainView.EnterView> implements MainInteractor.MainPresenter.EnterPresenter{

    FragmentEnterView parent;

    public FragmentEnterPresenter(FragmentEnterView parent){
        this.parent = parent;
        super.onAttach(this.parent, this.parent.getContext());
    }

    @Override
    public void callContent() {
        Apps.getInstance().getPreferenceHelper().setBooleanPref(Constant.flagDoEnter, true);
        parent.startActivity(new Intent(getInstance().getApplicationContext(), ContentsView.class));
        ActivityCompat.finishAfterTransition(parent.parentActivity);
    }
}
