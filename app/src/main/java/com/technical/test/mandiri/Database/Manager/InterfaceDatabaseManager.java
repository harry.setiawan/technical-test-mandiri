package com.technical.test.mandiri.Database.Manager;


import com.technical.test.mandiri.Database.Model.Categories;
import com.technical.test.mandiri.Database.Model.KeywordSearch;

import org.greenrobot.greendao.query.WhereCondition;

import java.util.List;

public interface InterfaceDatabaseManager {

    void AddCategories(List<Categories> data);
    void UpdateCategoriesFavorSingleCondition(WhereCondition condition1, boolean favor);
    List<Categories> ListCategoriesSingleCondition(WhereCondition condition1);
    List<Categories> ListAllCategories(int start, int limit);
    int CountCategories();
    void ClearCategories();

    void AddKeywordSearch(KeywordSearch data);
    List<KeywordSearch> ListKeywordSearchSingleCondition(WhereCondition condition1);
    List<KeywordSearch> ListAllKeywordSearch(WhereCondition condition, int limit);
    int CountKeywordSearch();
    void ClearKeywordSearch();
    
    void openReadableDb();
    void openWritableDb();
    void closeDbConnections();
    void dropDatabase();
}
