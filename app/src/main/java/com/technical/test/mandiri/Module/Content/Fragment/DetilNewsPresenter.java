package com.technical.test.mandiri.Module.Content.Fragment;

import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Module.Content.ContentInteractor;

public class DetilNewsPresenter extends BasePresenterImpl<ContentInteractor.ContentView.DetilNewsView> implements ContentInteractor.ContentPresenter.DetilNewsPresenter{

    FragmentDetilNewsView parent;

    public DetilNewsPresenter(FragmentDetilNewsView parent){
        this.parent = parent;
        super.onAttach(this.parent, this.parent.getContext());
    }

}
