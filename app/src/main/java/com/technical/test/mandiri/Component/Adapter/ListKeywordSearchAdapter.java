package com.technical.test.mandiri.Component.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technical.test.mandiri.Component.Base.BaseActivity;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Database.Model.KeywordSearch;
import com.technical.test.mandiri.R;

import java.util.List;

public class ListKeywordSearchAdapter extends RecyclerView.Adapter<ListKeywordSearchAdapter.Holder> {
    List<KeywordSearch> itemList;
    BaseActivity ctx;
    private LayoutInflater inflater;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;

    private itemClickCallback itemClickCallback;

    public ListKeywordSearchAdapter(List<KeywordSearch> data, BaseActivity ctx){
        this.itemList = data;
        this.ctx = ctx;
        this.inflater = LayoutInflater.from(ctx);
        mSelectedItemsIds = new SparseBooleanArray();
    }

    public List<KeywordSearch> getItemList() {
        return itemList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_keyword, parent, false);

        Holder viewHolder = new Holder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.name.setText(itemList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void toggleSelection(int position) {
        selectView(position, false);
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            itemList.remove(position);
        }
        notifyDataSetChanged();
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public LinearLayout itemLayout;
        public TextView name;

        public Holder(View itemView) {
            super(itemView);
            itemLayout = (LinearLayout) itemView.findViewById(R.id.item);
            name = (TextView) itemView.findViewById(R.id.name);
            name.setTypeface(Utils.typefaceBold());
            itemLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View itemView) {
            switch (itemView.getId()){
                case R.id.item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
            }
        }
    }

    public interface itemClickCallback{
        void onItemClick(int p);
    }

    public void setItemClickCallback(final itemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }
}