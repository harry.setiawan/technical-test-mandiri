package com.technical.test.mandiri.Module.Search;

public interface SearchInteractor {
    interface SearchPresenter {
        void OnLoadInitialFragment();

        interface KeywordPresenter {
            void OnLoadKeywordSearch(String text);
        }
    }

    interface SearchView {
        void initText();
        void initTypeface();
        void onSetToolbar();

        interface KeywordView {
            void declareAdapter();
        }
    }
}
