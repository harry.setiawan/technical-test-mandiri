package com.technical.test.mandiri.Module.Main.Fragment;


import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Module.Main.MainInteractor;

public class FragmentSplashPresenter extends BasePresenterImpl<MainInteractor.MainView.SplashView> implements MainInteractor.MainPresenter.SplashPresenter{

    FragmentSplashView parent;

    public FragmentSplashPresenter(FragmentSplashView parent){
        this.parent = parent;
    }

}
