package com.technical.test.mandiri.Database.Model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Categories {
    @Id
    private String categoryId;
    private String sourceId;
    private String sourceName;
    private String author;
    private String title;
    private String desctiption;
    private String url;
    private String urlToImage;
    private String publishedAt;
    private String content;
    private Long  categoryRecord;
    @Generated(hash = 1358831705)
    public Categories(String categoryId, String sourceId, String sourceName,
            String author, String title, String desctiption, String url,
            String urlToImage, String publishedAt, String content,
            Long categoryRecord) {
        this.categoryId = categoryId;
        this.sourceId = sourceId;
        this.sourceName = sourceName;
        this.author = author;
        this.title = title;
        this.desctiption = desctiption;
        this.url = url;
        this.urlToImage = urlToImage;
        this.publishedAt = publishedAt;
        this.content = content;
        this.categoryRecord = categoryRecord;
    }
    @Generated(hash = 267348489)
    public Categories() {
    }
    public String getCategoryId() {
        return this.categoryId;
    }
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
    public String getSourceId() {
        return this.sourceId;
    }
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }
    public String getSourceName() {
        return this.sourceName;
    }
    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
    public String getAuthor() {
        return this.author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDesctiption() {
        return this.desctiption;
    }
    public void setDesctiption(String desctiption) {
        this.desctiption = desctiption;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getUrlToImage() {
        return this.urlToImage;
    }
    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }
    public String getPublishedAt() {
        return this.publishedAt;
    }
    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }
    public String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public Long getCategoryRecord() {
        return this.categoryRecord;
    }
    public void setCategoryRecord(Long categoryRecord) {
        this.categoryRecord = categoryRecord;
    }

}
