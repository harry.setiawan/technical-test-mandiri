package com.technical.test.mandiri.Module.Content.Fragment;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Module.Content.ContentInteractor;
import com.technical.test.mandiri.Module.Main.MainView;

import static com.technical.test.mandiri.Apps.getInstance;

public class AboutPresenter extends BasePresenterImpl<ContentInteractor.ContentView.AboutView> implements ContentInteractor.ContentPresenter.AboutPresenter{

    FragmentAboutView parent;

    public AboutPresenter(FragmentAboutView parent){
        this.parent = parent;
        super.onAttach(this.parent, this.parent.getContext());
    }

    @Override
    public void OnExit() {
        Constant.flagNavigationMenu = 1;
        Apps.getInstance().getDatabase().dropDatabase();
        Apps.getInstance().getPreferenceHelper().setBooleanPref(Constant.flagDoEnter, false);
        parent.startActivity(new Intent(getInstance().getApplicationContext(), MainView.class));
        ActivityCompat.finishAfterTransition(parent.parentActivity);
    }
}
