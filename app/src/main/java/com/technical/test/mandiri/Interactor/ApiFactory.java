package com.technical.test.mandiri.Interactor;

import android.content.Context;
import android.os.Build;

import com.technical.test.mandiri.BuildConfig;

import java.io.File;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class ApiFactory {
  private static final long UNIVERSAL_TIMEOUT = 60;

  public ApiInterface getApi(Context mContext) {
    return new Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL_PUBLIC)
            .client(mClient(mContext))
            .client(getUnsafeOkHttpClient().build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
            .create(ApiInterface.class);
  }

  public static OkHttpClient.Builder getUnsafeOkHttpClient() {
    try {
      // Create a trust manager that does not validate certificate chains
      final TrustManager[] trustAllCerts = new TrustManager[]{
              new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                  return new java.security.cert.X509Certificate[]{};
                }
              }
      };

      // Install the all-trusting trust manager
      final SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

      // Create an ssl socket factory with our all-trusting manager
      final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

      OkHttpClient.Builder builder = new OkHttpClient.Builder();
      builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
      builder.hostnameVerifier(new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      });
      return builder;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private OkHttpClient mClient(Context mContext) {
    Cache cache = null;
    try {
      cache = new Cache(new File(mContext.getCacheDir(), "cache"), 10 * 1024 * 1024); // 10 MB
    } catch (Exception e) {
      Timber.e(e, "Error creating cache file");
    }
    OkHttpClient client = null;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      client =  new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor())
              .connectTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
              .writeTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
              .readTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
              .cache(cache)
              .build();
    } else {
      client =new OkHttpClient();
      try {
        client = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor())
                .connectTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
                .sslSocketFactory(new TLSSocketFactory())
                .cache(cache)
                .build();
      } catch (KeyManagementException e) {
        e.printStackTrace();
      } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
    }

    return client;
  }

  private HttpLoggingInterceptor httpLoggingInterceptor() {
    HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message -> Timber.d(message));
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return httpLoggingInterceptor;
  }
}
