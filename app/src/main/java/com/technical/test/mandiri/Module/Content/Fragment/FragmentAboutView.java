package com.technical.test.mandiri.Module.Content.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.technical.test.mandiri.Component.Adapter.ListCategoriesAdapter;
import com.technical.test.mandiri.Component.Base.BaseFragmentPresenter;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Module.Content.ContentInteractor;
import com.technical.test.mandiri.Module.Content.ContentsView;
import com.technical.test.mandiri.R;

import butterknife.BindView;

public class FragmentAboutView extends BaseFragmentPresenter<AboutPresenter> implements
        ContentInteractor.ContentView.AboutView, View.OnClickListener {

    @BindView(R.id.tvTitle)
    public TextView title;
    @BindView(R.id.tvDesc)
    public TextView desc;
    @BindView(R.id.tvThanks)
    public TextView thanks;
    @BindView(R.id.btnExit)
    public Button exit;

    ListCategoriesAdapter NewsAdapter;
    ContentsView parentActivity;

    public FragmentAboutView(){ }

    public static FragmentAboutView newInstance()
    {
        return new FragmentAboutView();
    }

    @Override
    protected AboutPresenter initPresenter() {
        return new AboutPresenter(this);
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_about;
    }

    @Override
    protected void onInitialize(@Nullable Bundle savedInstanceState) {
        this.parentActivity = (ContentsView) getActivity();
        initTypeface();
        exit.setOnClickListener(this);
    }

    @Override
    public void initTypeface() {
        title.setTypeface(Utils.typefaceBold());
        desc.setTypeface(Utils.typefaceNormal());
        thanks.setTypeface(Utils.typefaceNormal());
        exit.setTypeface(Utils.typefaceNormal());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnExit:
                mPresenter.OnExit();
                break;
            default:
                break;
        }
    }
}
