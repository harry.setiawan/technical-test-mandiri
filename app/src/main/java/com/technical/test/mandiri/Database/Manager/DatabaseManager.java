package com.technical.test.mandiri.Database.Manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.technical.test.mandiri.BuildConfig;
import com.technical.test.mandiri.Database.Model.Categories;
import com.technical.test.mandiri.Database.Model.CategoriesDao;
import com.technical.test.mandiri.Database.Model.DaoMaster;
import com.technical.test.mandiri.Database.Model.DaoSession;
import com.technical.test.mandiri.Database.Model.KeywordSearch;
import com.technical.test.mandiri.Database.Model.KeywordSearchDao;

import org.greenrobot.greendao.async.AsyncOperation;
import org.greenrobot.greendao.async.AsyncOperationListener;
import org.greenrobot.greendao.async.AsyncSession;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


public class DatabaseManager implements InterfaceDatabaseManager, AsyncOperationListener {

    private String DATABASE_PATH = null;

    private static DatabaseManager instance;
    private Context context;

    //DB REALTIME
    private String DATABASE_NAME = BuildConfig.BASE_DB_NAME;
    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase database;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private AsyncSession asyncSession;
    private List<AsyncOperation> completedOperations;

    public DatabaseManager(final Context context) {
        this.context = context;
        DatabaseGenerator();
    }

    private void DatabaseGenerator() {
        DATABASE_PATH = "/data/data/" + context.getPackageName() + "/db/";

        mHelper = new DaoMaster.DevOpenHelper(this.context, DATABASE_PATH + DATABASE_NAME, null);
        completedOperations = new CopyOnWriteArrayList<AsyncOperation>();
        database = mHelper.getWritableDatabase();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
    }
    @Override
    public void onAsyncOperationCompleted(AsyncOperation operation) {
        completedOperations.add(operation);
    }

    @Override
    public void closeDbConnections() {
        if (instance != null) {
            instance = null;
        }
        if (daoSession != null) {
            daoSession.clear();
            daoSession = null;
        }
        if (database != null && database.isOpen()) {
            database.close();
        }
        if (mHelper != null) {
            mHelper.close();
            mHelper = null;
        }
        if (instance != null) {
            instance = null;
        }
    }

    @Override
    public void AddCategories(List<Categories> data) {
        try {
            if (data != null) {
                openWritableDb();
                CategoriesDao dDao = daoSession.getCategoriesDao();
                dDao.insertOrReplaceInTx(data);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void UpdateCategoriesFavorSingleCondition(WhereCondition condition1, boolean favor) {
        Categories d = null;
        try {
            openReadableDb();
            CategoriesDao dDao = daoSession.getCategoriesDao();
            d = dDao.queryBuilder().where(condition1).unique();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Categories> ListCategoriesSingleCondition(WhereCondition condition1) {
        List<Categories> d = null;
        try {
            openReadableDb();
            CategoriesDao dDao = daoSession.getCategoriesDao();
            d = dDao.queryBuilder().where(condition1).orderAsc(CategoriesDao.Properties.CategoryId).list();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }

    @Override
    public List<Categories> ListAllCategories(int start, int limit) {
        List<Categories> d = null;
        try {
            openReadableDb();
            CategoriesDao dDao = daoSession.getCategoriesDao();
            d = dDao.queryBuilder().orderAsc(CategoriesDao.Properties.CategoryId).offset(start).limit(limit).list();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }

    @Override
    public int CountCategories() {
        int d = 0;
        try {
            openReadableDb();
            CategoriesDao dDao = daoSession.getCategoriesDao();
            d = dDao.queryBuilder().list().size();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }

    @Override
    public void ClearCategories() {
        try {
            openWritableDb();
            CategoriesDao dDao = daoSession.getCategoriesDao();
            dDao.deleteAll();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void AddKeywordSearch(KeywordSearch data) {
        try {
            if (data != null) {
                openWritableDb();
                KeywordSearchDao dDao = daoSession.getKeywordSearchDao();
                dDao.insertOrReplaceInTx(data);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<KeywordSearch> ListKeywordSearchSingleCondition(WhereCondition condition1) {
        List<KeywordSearch> d = null;
        try {
            openReadableDb();
            KeywordSearchDao dDao = daoSession.getKeywordSearchDao();
            d = dDao.queryBuilder().where(condition1).orderAsc(KeywordSearchDao.Properties.Id).list();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }

    @Override
    public List<KeywordSearch> ListAllKeywordSearch(WhereCondition condition, int limit) {
        List<KeywordSearch> d = null;
        try {
            openReadableDb();
            KeywordSearchDao dDao = daoSession.getKeywordSearchDao();
            d = dDao.queryBuilder().where(condition).orderDesc(KeywordSearchDao.Properties.Id).limit(limit).list();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }

    @Override
    public int CountKeywordSearch() {
        int d = 0;
        try {
            openReadableDb();
            KeywordSearchDao dDao = daoSession.getKeywordSearchDao();
            d = dDao.queryBuilder().list().size();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }

    @Override
    public void ClearKeywordSearch() {
        try {
            openWritableDb();
            KeywordSearchDao dDao = daoSession.getKeywordSearchDao();
            dDao.deleteAll();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void openReadableDb() throws SQLiteException {
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }

    @Override
    public void openWritableDb() throws SQLiteException {
        daoMaster   = new DaoMaster(database);
        daoSession  = daoMaster.newSession();
        asyncSession= daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }

    @Override
    public void dropDatabase() {
        try {
            openWritableDb();
            ClearCategories();
            ClearKeywordSearch();

            DatabaseGenerator();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
