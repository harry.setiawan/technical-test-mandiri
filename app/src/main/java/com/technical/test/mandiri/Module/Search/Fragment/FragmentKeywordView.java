package com.technical.test.mandiri.Module.Search.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.InputMethodManager;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Adapter.ListKeywordSearchAdapter;
import com.technical.test.mandiri.Component.Base.BaseFragmentPresenter;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Module.Content.ContentsView;
import com.technical.test.mandiri.Module.Search.SearchInteractor;
import com.technical.test.mandiri.Module.Search.SearchsView;
import com.technical.test.mandiri.R;

import butterknife.BindView;

public class FragmentKeywordView extends BaseFragmentPresenter<KeywordPresenter> implements
        SearchInteractor.SearchView.KeywordView,
        ListKeywordSearchAdapter.itemClickCallback{

    @BindView(R.id.refresh)
    SwipeRefreshLayout swRefresh;
    @BindView(R.id.recyclerView)
    public RecyclerView recyclerView;

    ListKeywordSearchAdapter keywordSearchAdapter;
    SearchsView parentActivity;

    public FragmentKeywordView(){ }

    public static FragmentKeywordView newInstance()
    {
        return new FragmentKeywordView();
    }

    @Override
    protected KeywordPresenter initPresenter() {
        return new KeywordPresenter(this);
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_keyword;
    }

    @Override
    protected void onInitialize(@Nullable Bundle savedInstanceState) {
        this.parentActivity = (SearchsView) getActivity();
        declareAdapter();
        loadSearch("");
    }

    public void loadSearch(String text) {
        mPresenter.OnLoadKeywordSearch(text);
    }

    @Override
    public void declareAdapter() {
        LinearLayoutManager LLayout = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(LLayout);
        recyclerView.addItemDecoration(new DividerItemDecoration(parentActivity, DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        keywordSearchAdapter = new ListKeywordSearchAdapter(mPresenter.keywordSearches, parentActivity);
        keywordSearchAdapter.setItemClickCallback(this);
        recyclerView.setAdapter(keywordSearchAdapter);
    }

    @Override
    public void onItemClick(int position) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) Apps.getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(parentActivity.etSearch.getWindowToken(), 0);
            }
        },500);
        Constant.flagNavigationMenu = 1;
        Intent theIntent = new Intent(Apps.getInstance(), ContentsView.class);
        theIntent.putExtra("search", mPresenter.keywordSearches.get(position).getName());
        startActivity(theIntent);
    }
}
