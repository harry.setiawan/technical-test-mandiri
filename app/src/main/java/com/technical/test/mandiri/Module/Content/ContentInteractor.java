package com.technical.test.mandiri.Module.Content;

public interface ContentInteractor {
    interface ContentPresenter {
        void OnLoadInitialFragment();
        void OnLoadInitialNewsFragment();
        void OnLoadInitialDetilNewsFragment();
        void OnLoadInitialAboutFragment();

        interface NewsPresenter {
            void OnRequestNews();
        }

        interface DetilNewsPresenter {
        }

        interface AboutPresenter {
            void OnExit();
        }

        interface ContentDetilNewsPresenter {
        }
    }

    interface ContentView {
        void initText();
        void initTypeface();
        void callAlertDialog(String content, String button);

        void onSetToolbar();

        interface NewsView {
            void initTypeface();
            void setSwipeRefreshColor();
            void declareAdapter();
        }

        interface DetilNewsView {
            void initTypeface();
            void declareAdapter();
        }

        interface AboutView {
            void initTypeface();
        }

    }
}
