package com.technical.test.mandiri.Module.Search;

import android.support.v4.app.FragmentTransaction;

import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Module.Search.Fragment.FragmentKeywordView;
import com.technical.test.mandiri.R;

public class SearchsPresenter extends BasePresenterImpl<SearchInteractor.SearchView> implements SearchInteractor.SearchPresenter {

    SearchsView parent;
    public FragmentKeywordView keywordView = new FragmentKeywordView();

    public SearchsPresenter(SearchsView parentActivity){
        this.parent = parentActivity;
        onAttach(this.parent, this.parent.getApplicationContext());
    }

    @Override
    public void OnLoadInitialFragment() {
        FragmentTransaction fragmentTransaction = parent.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, keywordView);
        fragmentTransaction.commit();
    }
}
