package com.technical.test.mandiri.Interactor;

import com.technical.test.mandiri.Interactor.Response.DataNews;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiInterface {

    @GET("https://newsapi.org/v2/top-headlines")
    Observable<DataNews> requestNews(@Query("country") String country, @Query("category") String category, @Query("page") int page, @Query("apiKey") String apiKey);
}
