package com.technical.test.mandiri.Database.Model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class KeywordSearch {
    @Id
    private String id;
    private String name;
    @Generated(hash = 1060945804)
    public KeywordSearch(String id, String name) {
        this.id = id;
        this.name = name;
    }
    @Generated(hash = 1035499732)
    public KeywordSearch() {
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
