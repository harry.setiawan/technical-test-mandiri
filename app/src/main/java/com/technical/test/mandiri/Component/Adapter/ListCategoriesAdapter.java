package com.technical.test.mandiri.Component.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BaseActivity;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Database.Model.Categories;
import com.technical.test.mandiri.R;

import java.util.List;

public class ListCategoriesAdapter extends RecyclerView.Adapter<ListCategoriesAdapter.Holder> {
    List<Categories> itemList;
    BaseActivity ctx;
    private LayoutInflater inflater;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;

    private itemClickCallback itemClickCallback;

    public ListCategoriesAdapter(List<Categories> data, BaseActivity ctx){
        this.itemList = data;
        this.ctx = ctx;
        this.inflater = LayoutInflater.from(ctx);
        mSelectedItemsIds = new SparseBooleanArray();
    }

    public List<Categories> getItemList() {
        return itemList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = null;
        int orientation = Apps.getInstance().getResources().getConfiguration().orientation;
        itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_content, parent, false);

        Holder viewHolder = new Holder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.image.setImageDrawable(Apps.getInstance().getResources().getDrawable(R.mipmap.no_available));
        if(Utils.isConnected()){
            if(itemList.get(position).getUrlToImage() == null) {
            }else{
                Picasso.with(
                        Apps.getInstance().getApplicationContext())
                        .load(itemList.get(position).getUrlToImage())
                        .resize(700, 300)
                        .centerCrop()
                        .into(holder.image);
            }
        }

        String description = "";
        if(itemList.get(position).getDesctiption().equals("")) {
            if(itemList.get(position).getDesctiption().length() > 150) {
                description = (itemList.get(position).getDesctiption()).substring(0,150) + " ...";
            }else{
                description = (itemList.get(position).getDesctiption());
            }
        }else{
            if(itemList.get(position).getDesctiption().length() > 150) {
                description = (itemList.get(position).getDesctiption()).substring(0,150) + " ...";
            }else{
                description = (itemList.get(position).getDesctiption());
            }
        }
        holder.title.setText(itemList.get(position).getTitle());
        holder.snippet.setText(description.trim());
        holder.date.setText("-");
        holder.time.setText(itemList.get(position).getPublishedAt());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void toggleSelection(int position) {
        selectView(position, false);
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            itemList.remove(position);
        }
        notifyDataSetChanged();
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public LinearLayout itemLayout;
        public ImageView image;
        public TextView title;
        public TextView date;
        public TextView time;
        public TextView snippet;

        public Holder(View itemView) {
            super(itemView);
            itemLayout = (LinearLayout) itemView.findViewById(R.id.item);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            snippet = (TextView) itemView.findViewById(R.id.snippet);

            title.setTypeface(Utils.typefaceBold());
            date.setTypeface(Utils.typefaceOblique());
            time.setTypeface(Utils.typefaceOblique());
            snippet.setTypeface(Utils.typefaceNormal());
            itemLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View itemView) {
            switch (itemView.getId()){
                case R.id.item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
            }
        }
    }

    public interface itemClickCallback{
        void onItemClick(int p);
        void onItemFavorClick(int p);
    }

    public void setItemClickCallback(final itemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }
}