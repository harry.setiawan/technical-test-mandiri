package com.technical.test.mandiri.Interactor.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataNews {
  @SerializedName("status") private String status;
  @SerializedName("articles") private List<DataCategories> response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DataCategories> getResponse() {
        return response;
    }

    public void setResponse(List<DataCategories> response) {
        this.response = response;
    }
}
