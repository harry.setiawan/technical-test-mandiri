package com.technical.test.mandiri.Module.Content.Fragment;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Database.Model.Categories;
import com.technical.test.mandiri.Interactor.ApiCore;
import com.technical.test.mandiri.Interactor.Response.ObjRequestCallback;
import com.technical.test.mandiri.Module.Content.ContentInteractor;

import java.util.ArrayList;
import java.util.List;

public class NewsPresenter extends BasePresenterImpl<ContentInteractor.ContentView.NewsView> implements ContentInteractor.ContentPresenter.NewsPresenter{

    List<Categories> News = new ArrayList<>();

    FragmentNewsView parent;

    public NewsPresenter(FragmentNewsView parent){
        this.parent = parent;
        super.onAttach(this.parent, this.parent.getContext());
    }

    @Override
    public void OnRequestNews() {
        parent.swRefresh.setRefreshing(true);
        if(Utils.isConnected()) {
            if (Constant.searchReset) {
                Apps.getInstance().getDatabase().ClearCategories();
                Constant.searchReset = false;
            }
            Apps.getInstance().getAPICore().doGetNews(
                    Constant.searchText, parent.parentActivity.currentPage, new ApiCore.APICallback() {
                        @Override
                        public void onResponseSuccess(ObjRequestCallback objRequestCallback) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    parent.swRefresh.setRefreshing(false);
                                    if (parent.parentActivity.currentPage == 0) {
                                        News.addAll(Apps.getInstance().getDatabase().ListAllCategories(0, 10));
                                    } else {
                                        News.addAll(Apps.getInstance().getDatabase().ListAllCategories((parent.parentActivity.currentPage) * parent.parentActivity.perPage, (parent.parentActivity.currentPage + 1) * parent.parentActivity.perPage));
                                    }
                                    parent.NewsAdapter.notifyDataSetChanged();
                                }
                            }, 200);
                        }

                        @Override
                        public void onResponseFailed(ObjRequestCallback objRequestCallback) {
                            parent.swRefresh.setRefreshing(false);
                        }

                        @Override
                        public void onResponseError(ObjRequestCallback objRequestCallback) {
                            parent.swRefresh.setRefreshing(false);
                        }
                    }
            );
        }else{
            parent.swRefresh.setRefreshing(false);
            News.addAll(Apps.getInstance().getDatabase().ListAllCategories(0, 10));
            parent.NewsAdapter.notifyDataSetChanged();
        }
    }

}
