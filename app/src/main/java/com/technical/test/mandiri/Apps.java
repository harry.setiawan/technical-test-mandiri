package com.technical.test.mandiri;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.technical.test.mandiri.Component.PreferencesManager;
import com.technical.test.mandiri.Component.Service.ConnectivityChangeReceiver;
import com.technical.test.mandiri.Database.Manager.DatabaseManager;
import com.technical.test.mandiri.Database.Manager.DatabaseMapper;
import com.technical.test.mandiri.Database.Manager.InterfaceDatabaseManager;
import com.technical.test.mandiri.Interactor.ApiCore;
import com.technical.test.mandiri.Interactor.ApiFactory;
import com.technical.test.mandiri.Interactor.ApiInterface;

public class Apps extends Application implements
        ConnectivityChangeReceiver.ConnectivityReceiverListener {
  private static Apps instance;
  private ApiInterface mApiInterface;
  private DatabaseMapper dbMaper;
  private PreferencesManager mPreferenceHelper;
  private InterfaceDatabaseManager DB;
  private ApiCore coreRequestAPI;

  public BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {

    }
  };

  @Override
  public void onCreate() {
    super.onCreate();

    if (instance == null) instance = this;
  }
  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  public static Apps getInstance() {
    return instance;
  }

  public ApiInterface getAPIInterface() {
    if (mApiInterface == null) {
      mApiInterface = new ApiFactory().getApi(this);
    }
    return mApiInterface;
  }

  public ApiCore getAPICore() {
    if (coreRequestAPI == null) {
      coreRequestAPI = new ApiCore();
    }
    return coreRequestAPI;
  }

  public InterfaceDatabaseManager getDatabase(){
    if(DB == null) {
      DB = new DatabaseManager(getApplicationContext());
    }
    return DB;
  }

  public DatabaseMapper getDatabaseMapper() {
    if (dbMaper == null) {
      dbMaper = new DatabaseMapper();
    }
    return dbMaper;
  }

  public PreferencesManager getPreferenceHelper() {
    if (mPreferenceHelper == null) {
      mPreferenceHelper = new PreferencesManager(this);
    }
    return mPreferenceHelper;
  }

  public boolean isNetworkAvailable() {
    ConnectivityManager connectivityManager =
        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
  }

  public String getIMEI(){
    try {
      TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
      return telephonyManager.getDeviceId();
    }catch (Exception e){
      return "";
    }
  }

  public void showToast(String content){
    Toast.makeText(this, content, Toast.LENGTH_LONG).show();
  }

  @Override
  public void onNetworkConnectionChanged(boolean isConnected){
  }
}
