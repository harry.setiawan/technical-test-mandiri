package com.technical.test.mandiri.Module.Content.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.webkit.WebView;
import android.widget.TextView;

import com.technical.test.mandiri.Component.Adapter.ListCategoriesAdapter;
import com.technical.test.mandiri.Component.Base.BaseFragmentPresenter;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Module.Content.ContentInteractor;
import com.technical.test.mandiri.Module.Content.ContentsView;
import com.technical.test.mandiri.R;

import butterknife.BindView;

public class FragmentDetilNewsView extends BaseFragmentPresenter<DetilNewsPresenter> implements
        ContentInteractor.ContentView.DetilNewsView {

    @BindView(R.id.webview)
    public WebView webView;

    ListCategoriesAdapter NewsAdapter;
    ContentsView parentActivity;

    public FragmentDetilNewsView(){ }

    public static FragmentDetilNewsView newInstance()
    {
        return new FragmentDetilNewsView();
    }

    @Override
    protected DetilNewsPresenter initPresenter() {
        return new DetilNewsPresenter(this);
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_detil_news;
    }

    @Override
    protected void onInitialize(@Nullable Bundle savedInstanceState) {
        this.parentActivity = (ContentsView) getActivity();
//        webView.loadData();
    }

    @Override
    public void initTypeface() {
    }

    @Override
    public void declareAdapter() {
    }
}
