package com.technical.test.mandiri.Module.Search.Fragment;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Database.Model.KeywordSearch;
import com.technical.test.mandiri.Database.Model.KeywordSearchDao;
import com.technical.test.mandiri.Module.Search.SearchInteractor;

import java.util.ArrayList;
import java.util.List;

public class KeywordPresenter extends BasePresenterImpl<SearchInteractor.SearchView.KeywordView> implements SearchInteractor.SearchPresenter.KeywordPresenter{

    List<KeywordSearch> keywordSearches = new ArrayList<>();

    FragmentKeywordView parent;

    public KeywordPresenter(FragmentKeywordView parent){
        this.parent = parent;
        super.onAttach(this.parent, this.parent.getContext());
    }

    @Override
    public void OnLoadKeywordSearch(String text) {
        keywordSearches.clear();
        keywordSearches.addAll(Apps.getInstance().getDatabase().ListAllKeywordSearch(KeywordSearchDao.Properties.Name.like("%"+text+"%"), 10));
        parent.keywordSearchAdapter.notifyDataSetChanged();
    }
}
