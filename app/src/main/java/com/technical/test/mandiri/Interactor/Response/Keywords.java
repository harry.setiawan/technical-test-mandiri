package com.technical.test.mandiri.Interactor.Response;

import com.google.gson.annotations.SerializedName;

class Keywords {
    @SerializedName("name") private String name;
    @SerializedName("value") private String value;
    @SerializedName("rank") private int rank;
}
