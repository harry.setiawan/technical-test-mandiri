package com.technical.test.mandiri.Module.Content;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BaseActivityPresenter;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Module.Search.SearchsView;
import com.technical.test.mandiri.R;

import butterknife.BindView;

import static com.technical.test.mandiri.Apps.getInstance;

public class ContentsView extends BaseActivityPresenter<ContentPresenter>
        implements ContentInteractor.ContentView,
        NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {

    public int currentPage = 0;
    public int currentPageDetil = 0;
    public int perPage = 10;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @BindView(R.id.layoutSearch)
    public LinearLayout layoutSearch;
    @BindView(R.id.layoutToolbar)
    public RelativeLayout layoutToolbar;
    @BindView(R.id.layoutContent)
    public LinearLayout layoutContent;
    @BindView(R.id.etSearch)
    public EditText etSearch;
    @BindView(R.id.btn_navigation)
    public BottomNavigationView navigation;

    public ImageView toolbarIcon;
    public TextView toolbarTitle;
    public ImageView toolbarAction;

    public android.support.v4.app.FragmentManager fragmentManager;

    @Override
    protected int DeclareLayout() {
        return R.layout.activity_content;
    }

    @Override
    protected ContentPresenter DeclarePresenter() {
        return new ContentPresenter(this);
    }

    @Override
    protected void onInitialize(Bundle savedInstance) {
        getDataSearch();
        onSetToolbar();
        fragmentManager = getSupportFragmentManager();
        mPresenter.OnLoadInitialFragment();
        initTypeface();
        initText();
        navigation.setOnNavigationItemSelectedListener(this);
    }

    private void getDataSearch() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Constant.searchText = extras.getString("search");
            Constant.searchReset = true;
        }
    }

    @Override
    public void initText() {
    }

    @Override
    public void initTypeface() {
        etSearch.setTypeface(Utils.typefaceNormal());
    }

    @Override
    public void onSetToolbar() {
        setSupportActionBar(toolbar);
        toolbarIcon = toolbar.findViewById(R.id.toolbarImage);
        toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarAction = toolbar.findViewById(R.id.btnSearch);
        toolbarIcon.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
        toolbarTitle.setTypeface(Utils.typefaceBold());
        toolbarAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getInstance().getApplicationContext(), SearchsView.class));
            }
        });

    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void callAlertDialog(String content, String button){
        showAlert(content, button, new itemClickCallbackDialog() {
            @Override
            public void onItemClick(boolean action) {
                onResume();
            }
        });
    }

    @Override
    public void onResume() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(Apps.getInstance().networkChangeReceiver, intentFilter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(Apps.getInstance().networkChangeReceiver);
    }

    @Override
    public void onBackPressed() {
        if(Constant.DetilNews){
            Constant.DetilNews = false;
            Constant.flagNavigationMenu = 1;
            mPresenter.OnLoadInitialFragment();
        }else if(Constant.DetilFavorNews){
            Constant.DetilFavorNews = false;
            Constant.flagNavigationMenu = 2;
            mPresenter.OnLoadInitialFragment();
        }else{
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_menu:
                Constant.flagNavigationMenu = 1;
                break;
            case R.id.about_menu:
                Constant.flagNavigationMenu = 3;
                break;
        }
        mPresenter.OnLoadInitialFragment();
        return true;
    }
}
