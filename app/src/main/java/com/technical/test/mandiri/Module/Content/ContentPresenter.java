package com.technical.test.mandiri.Module.Content;

import android.support.v4.app.FragmentTransaction;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BasePresenterImpl;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Module.Content.Fragment.FragmentAboutView;
import com.technical.test.mandiri.Module.Content.Fragment.FragmentDetilNewsView;
import com.technical.test.mandiri.Module.Content.Fragment.FragmentNewsView;
import com.technical.test.mandiri.R;

public class ContentPresenter extends BasePresenterImpl<ContentInteractor.ContentView> implements ContentInteractor.ContentPresenter {

    ContentsView parent;

    public ContentPresenter(ContentsView parentActivity){
        this.parent = parentActivity;
        onAttach(this.parent, this.parent.getApplicationContext());
    }

    @Override
    public void OnLoadInitialFragment() {
        if(Constant.flagNavigationMenu == 3){
            OnLoadInitialAboutFragment();
        }else{
            OnLoadInitialNewsFragment();
        }
    }

    @Override
    public void OnLoadInitialNewsFragment() {
        parent.toolbarTitle.setText(Apps.getInstance().getResources().getString(R.string.titleNews));
        FragmentNewsView newsView = new FragmentNewsView();
        FragmentTransaction fragmentTransaction = parent.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, newsView);
        fragmentTransaction.commit();
    }

    @Override
    public void OnLoadInitialDetilNewsFragment() {
        parent.toolbarTitle.setText(Apps.getInstance().getResources().getString(R.string.titleDetil));
        FragmentDetilNewsView newsView = new FragmentDetilNewsView();
        FragmentTransaction fragmentTransaction = parent.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, newsView);
        fragmentTransaction.commit();
    }

    @Override
    public void OnLoadInitialAboutFragment() {
        parent.toolbarTitle.setText(Apps.getInstance().getResources().getString(R.string.titleAbout));
        FragmentAboutView aboutView = new FragmentAboutView();
        FragmentTransaction fragmentTransaction = parent.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, aboutView);
        fragmentTransaction.commit();
    }

}
