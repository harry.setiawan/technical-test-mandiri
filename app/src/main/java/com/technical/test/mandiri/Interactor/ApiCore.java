package com.technical.test.mandiri.Interactor;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.BuildConfig;
import com.technical.test.mandiri.Component.Base.BaseSubscriber;
import com.technical.test.mandiri.Interactor.Response.DataNews;
import com.technical.test.mandiri.Interactor.Response.ObjRequestCallback;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ApiCore {

    CompositeSubscription mCompositeSubscription = new CompositeSubscription();
    public APICallback callback;
    ObjRequestCallback objRequestCallback;

    public void doGetNews(String search, int page, APICallback callback) {
        setAPICallback(callback);
        onSubscribeObservable(Apps.getInstance().getAPIInterface().requestNews("us", search, page, BuildConfig.BASE_API_KEY),
                new BaseSubscriber<DataNews>() {
            @Override
            public void onSuccess(DataNews response) {
                Apps.getInstance().getDatabaseMapper().mapNews(response.getResponse());
                objRequestCallback = new ObjRequestCallback(true, "success");
                callback.onResponseSuccess(objRequestCallback);
            }

            @Override
            public void onFailure(String message) {
                objRequestCallback = new ObjRequestCallback(false, message);
                callback.onResponseFailed(objRequestCallback);
            }

            @Override
            public void onError(String message) {
                objRequestCallback = new ObjRequestCallback(false, message);
                callback.onResponseError(objRequestCallback);
            }

            @Override
            public void onFinish() {
            }
        });
    }

    protected void onSubscribeObservable(Observable observable, Subscriber subscriber) {
        mCompositeSubscription.add(
                observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber)
        );
    }

    public interface APICallback{
        void onResponseSuccess(ObjRequestCallback objRequestCallback);
        void onResponseFailed(ObjRequestCallback objRequestCallback);
        void onResponseError(ObjRequestCallback objRequestCallback);
    }

    public void setAPICallback(final APICallback callback){
        this.callback = callback;
    }
}
