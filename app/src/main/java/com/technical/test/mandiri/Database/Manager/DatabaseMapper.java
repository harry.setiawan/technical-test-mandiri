package com.technical.test.mandiri.Database.Manager;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Database.Model.Categories;
import com.technical.test.mandiri.Database.Model.CategoriesDao;
import com.technical.test.mandiri.Interactor.Response.DataCategories;

import java.util.ArrayList;
import java.util.List;

public class DatabaseMapper {
    public boolean mapNews(List<DataCategories> data){
        List<Categories> categories = new ArrayList<>();
        Long CountCategory = Long.parseLong(String.valueOf(Apps.getInstance().getDatabase().CountCategories()));
        for(int i = 0 ; i < data.size() ; i++){
            categories.add(new Categories(
                    CountCategory+"",
                    data.get(i).getSource().getId(),
                    data.get(i).getSource().getName(),
                    data.get(i).getAuthor(),
                    data.get(i).getTitle(),
                    data.get(i).getDescription(),
                    data.get(i).getUrl(),
                    data.get(i).getUrlToImage(),
                    data.get(i).getPublishedAt(),
                    data.get(i).getContent(),
                    CountCategory));
            CountCategory++;
        }
        Apps.getInstance().getDatabase().AddCategories(categories);
        return true;
    }
}
