package com.technical.test.mandiri.Module.Search;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.technical.test.mandiri.Apps;
import com.technical.test.mandiri.Component.Base.BaseActivityPresenter;
import com.technical.test.mandiri.Component.Constant;
import com.technical.test.mandiri.Component.Utils;
import com.technical.test.mandiri.Database.Model.KeywordSearch;
import com.technical.test.mandiri.Module.Content.ContentsView;
import com.technical.test.mandiri.R;

import butterknife.BindView;

public class SearchsView extends BaseActivityPresenter<SearchsPresenter>
        implements SearchInteractor.SearchView {

        @BindView(R.id.toolbar)
        public Toolbar toolbar;
        @BindView(R.id.layoutSearch)
        public LinearLayout layoutSearch;
        @BindView(R.id.layoutToolbar)
        public RelativeLayout layoutToolbar;
        @BindView(R.id.layoutContent)
        public LinearLayout layoutContent;
        @BindView(R.id.etSearch)
        public EditText etSearch;
        @BindView(R.id.cancel)
        public ImageView cancel;

        public ImageView toolbarIcon;
        public TextView toolbarTitle;
        public ImageView toolbarAction;

        public android.support.v4.app.FragmentManager fragmentManager;

        @Override
        protected int DeclareLayout() {
                return R.layout.activity_search;
        }

        @Override
        protected SearchsPresenter DeclarePresenter() {
                return new SearchsPresenter(this);
        }

        @Override
        protected void onInitialize(Bundle savedInstance) {
                onSetToolbar();
                fragmentManager = getSupportFragmentManager();
                mPresenter.OnLoadInitialFragment();
                initTypeface();
                initText();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                                etSearch.requestFocus();
                                InputMethodManager imm = (InputMethodManager)   getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }
                },500);
        }

        @Override
        public void initText() {
                cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                                        }
                                },500);

                                Constant.flagNavigationMenu = 1;
                                Intent theIntent = new Intent(Apps.getInstance(), ContentsView.class);
                                theIntent.putExtra("search", "");
                                startActivity(theIntent);
                        }
                });
                etSearch.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void afterTextChanged(Editable s) {}
                        @Override
                        public void beforeTextChanged(CharSequence s, int start,int count, int after) {
                        }
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                mPresenter.keywordView.loadSearch(etSearch.getText().toString());
                        }
                });
                etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                        Constant.flagNavigationMenu = 1;
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                                        imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                                                }
                                        },500);
                                        Apps.getInstance().getDatabase().AddKeywordSearch(new KeywordSearch(Apps.getInstance().getDatabase().CountKeywordSearch()+"", etSearch.getText().toString()));
                                        Log.e("SSsS", Apps.getInstance().getDatabase().CountKeywordSearch()+"");
                                        Intent theIntent = new Intent(Apps.getInstance(), ContentsView.class);
                                        theIntent.putExtra("search", etSearch.getText().toString());
                                        startActivity(theIntent);

                                        return true;
                                }
                                return false;
                        }
                });
        }

        @Override
        public void initTypeface() {
                etSearch.setTypeface(Utils.typefaceNormal());
        }

        @Override
        public void onSetToolbar() {
                setSupportActionBar(toolbar);
                toolbarIcon = toolbar.findViewById(R.id.toolbarImage);
                toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
                toolbarAction = toolbar.findViewById(R.id.btnSearch);
                toolbarIcon.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
                toolbarTitle.setTypeface(Utils.typefaceBold());
        }

        @Override
        public void onClick(View v) {
        }

        @Override
        public void onResume() {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
                super.onResume();
        }

        @Override
        protected void onPause() {
                super.onPause();
        }

        @Override
        public void onBackPressed() {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
        }
}